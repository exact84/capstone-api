import { Request, Response, NextFunction, RequestHandler  } from 'express';
import {logger} from '../libs/logger';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
// export const errorHandler = (err: Error, req: Request, res: Response, next: NextFunction) => {
//   logger.error({ requestId: req.id },'Server Error:', err);
//   res.status(505).send({ message: 'Server Error', error: err.message });
// };


// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const errorLoggerMiddleware = (err: Error, req: Request, res: Response, next: NextFunction) => {
  logger.error(err.stack); // Логируем стэк ошибки
  res.status(505).send('Internal Server Error'); 
}

export const errorHandlerDecorator = (handler: RequestHandler) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    try {
      await handler(req, res, next); // Исполняем обработку роута
    } catch (error) {
      next(error); // Передаём обработку в errorLoggerMiddleware
    }
  };
}
