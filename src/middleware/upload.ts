import { Request, Response, NextFunction } from 'express';
import multer from 'multer';
import {logger} from '../libs/logger';

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public');
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage: storage });
const uploadMiddleware = upload.single('image');

// Обрабатываем загрузку файла
const handleFileUpload = (req: Request, res: Response, next: NextFunction) => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  uploadMiddleware(req, res, (err: any) => { // Уточняем типы для err и функции обратного вызова
    try {
      if (err) {
        logger.error({ requestId: req.id }, err);
        console.log('Avatar was not uploaded');
        return res.status(400).json({ error: err.message }); // Возвращаем ошибку клиенту
        // Продолжаем выполнение, не обновляя переменную пути к аватарке
      } 
      next();
    } catch (error) {
      logger.error({ requestId: req.id }, error);
      return res.status(505).json('Internal Server Error');
    }
  });
};

export default handleFileUpload;