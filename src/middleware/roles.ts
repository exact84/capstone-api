import { Request, Response, NextFunction } from 'express';
import jwt from "jsonwebtoken";
import {logger} from '../libs/logger';
import {config} from "../config/";
import { UserRole } from '../models/user.model';

interface DecodedData {
    email: string;
    role: UserRole;
  }

export const roles = (roles: UserRole[]) => {
  return (req: Request, res: Response, next: NextFunction) =>{  // Для передачи параметра role
    try {
        // console.info(`Check Role  ${roles}`);
        const token = req.headers.authorization.split(" ")[1];
        // if (!token) {        
        //   logger.error({ requestId: req.id }, 'Пользователь не авторизован. Токен не получен.');
        //   console.log({ requestId: req.id }, 'Пользователь не авторизован. Токен не получен.');
        //   return res
        //     .status(403)
        //     .json({ message: "Пользователь не авторизован. Токен не получен." });
        // } 
        // Проверяем токен
        const decodedData = jwt.verify(token, config.auth.secret) as DecodedData; 
                // const user = decodedData;
        if (!roles.includes(decodedData.role)) {       // Проверяем роль
            logger.error({ requestId: req.id }, 'Доступ только для админа.');
            return res
            .status(403)
            .json({ message: "Доступ только для админа." }); 
        }
        logger.info({ requestId: req.id }, `Доступ пользователя ${decodedData}`);
        next();
    } catch(err){
        logger.error(err); 
        res.status(505).json({Message: 'Ошибка при проверки роли'}) 
    }
  }
}