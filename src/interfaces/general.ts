import express from 'express';
import { AuthService } from '../services/auth.service';
import { UsersService } from '../services/users.service';
import { ProjectsService } from '../services/projects.service';
import { ExperienceService } from '../services/experience.service';
import { FeedbackService } from '../services/feedback.service';
import { User } from '../models/user.model';
import { Project } from '../models/project.model';
import { Experience } from '../models/experience.model';
import { Feedback } from '../models/feedback.model';

export interface Context {
  services: {
    authService: AuthService;   
    usersService: UsersService; 
    projectsService: ProjectsService;
    experienceService: ExperienceService;
    feedbackService: FeedbackService;
  };
}

export type RouterFactory = (context: Context) => express.Router;

export type Loader = (app: express.Application, context: Context) => void;

export interface Models {
  user: typeof User;
  project: typeof Project;
  experience: typeof Experience;
  feedback: typeof Feedback;
}
