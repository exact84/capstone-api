// import 'express';
import 'express-request-id';

declare module 'express' {
  interface Request {
    id?: string; // добавляем опциональное свойство id типа string
  }
}