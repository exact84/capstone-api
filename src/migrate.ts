import { runMigrations, rollbackMigrations } from './umzug';

const run = async () => {
  const action = process.argv[2]; // Получаем действие из аргументов командной строки

  switch (action) {
    case 'up':
      await runMigrations();
      console.log('Migrations are complete');
      break;
    case 'down':
      await rollbackMigrations();
      console.log('Rollback is complete');
      break;
    default:
      console.log('Invalid action. Use "up" to run migrations or "down" to rollback.');
      process.exit(1);
  }
};

run().catch((err) => {
  console.error('Migration process failed', err);
  process.exit(1);
});