import express from 'express';

import { Context } from '../interfaces/general';
import { makeAuthRouter } from '../routes/auth';
import { makeUsersRouter } from '../routes/users';
import { makeExperienceRouter } from '../routes/experience';
import { makeFeedbackRouter } from '../routes/feedback';
import { makeProjectsRouter } from '../routes/projects';
import passport from 'passport';

export const loadRoutes = (app: express.Router, context: Context) => {

  // Защита роутов от незалогиненых
// const isAuthenticated = function (req: Request, res:Response, next: NextFunction) {
//   if (req.isAuthenticated()) return next();
//   res.status(401).json({ error: 'Acces Denied' });
// };

  app.use('/api/auth', makeAuthRouter(context));
  app.use('/api/users', passport.authenticate('jwt', { session: false }));  // не используем сессии, потому что используем токен
  app.use('/api/users', makeUsersRouter(context));
  app.use('/api/experience', passport.authenticate('jwt', { session: false }));
  app.use('/api/experience', makeExperienceRouter(context));
  app.use('/api/experience', passport.authenticate('jwt', { session: false }));
  app.use('/api/feedback', makeFeedbackRouter(context));
  app.use('/api/experience', passport.authenticate('jwt', { session: false }));
  app.use('/api/projects', makeProjectsRouter(context));

};
