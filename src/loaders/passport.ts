import passport from "passport";
// import session from 'express-session';
import { Loader } from '../interfaces/general';
import {setupPassport} from "../libs/passport";
// import { Context } from "express-validator/src/context";
// import {config} from '../config';

export const loadPassport: Loader = (app, context) => {
  // app.use(session({
  //     secret: config.auth.secret ,
  //     resave: false,
  //     saveUninitialized: false,
  //   }));
  setupPassport( context.services.usersService );
  app.use(passport.initialize());
  // app.use(passport.session());

};
