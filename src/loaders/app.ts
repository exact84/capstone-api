import  { Request, Response, NextFunction } from 'express';
import requestId from 'express-request-id';
import { logger } from '../libs/logger';
import {loadMiddlewares} from './middlewares';
import {loadRoutes} from './routes';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const express = require('express');  // проблемы с ts-node
import {loadContext} from './context';
import {loadModels} from './models';
import {loadSequelize} from './sequelize';
import {config} from '../config';
import {loadPassport} from './passport';
import {errorLoggerMiddleware} from '../middleware/errorHandler';

export const loadApp = async () => {
  const app = express();
  const sequelize = loadSequelize(config)

  app.use(requestId()); //  для генерации уникального идентификатора запроса
  app.use(
    (req: Request & { id: string }, res: Response, next: NextFunction) => {
      // В каждой записи лога добавляем уникальный идентификатор запроса
      logger.info({ requestId: req.id }, 'Incoming request');
      next();
    },
  );

  loadModels(sequelize);

  const context = await loadContext();

  loadPassport(app, context);
  loadMiddlewares(app, context);
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

  loadRoutes(app, context);

  app.use(errorLoggerMiddleware);

  return app;
}
