import { Loader } from '../interfaces/general';
import upload from '../middleware/upload';

export const loadMiddlewares: Loader = (app) => {

    app.use(upload); // '/api/auth', - Если регистрация пользвателя то подготавляваем multer
    // app.use('/api/users', roles([UserRole.Admin]));  // Проверка на админство
};
