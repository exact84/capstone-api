import { Context } from '../interfaces/general';
import { AuthService } from '../services/auth.service';
import { UsersService } from '../services/users.service';
import { ExperienceService } from '../services/experience.service';
import { FeedbackService } from '../services/feedback.service';
import { ProjectsService } from '../services/projects.service';


export const loadContext = async (): Promise<Context> => {
  return {
    services: {
      authService: new AuthService(),
      usersService: new UsersService(),
      projectsService: new ProjectsService(),
      experienceService: new ExperienceService(),
      feedbackService: new FeedbackService()
    },
  };
};
