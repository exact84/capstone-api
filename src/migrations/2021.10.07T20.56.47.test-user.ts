import { MigrationFn } from 'umzug';
import { Sequelize } from 'sequelize';
import bcrypt from 'bcrypt';

export const up: MigrationFn<Sequelize> = async ({ context }) => {
  const q = context.getQueryInterface();

  // Хеширование
  const password = 'admin_password'; 
  const hashedPassword = await bcrypt.hash(password, 10);

  await q.bulkInsert('users', [{
    first_name: 'Admin',
    last_name: 'User',
    image: 'image.jpg',
    title: 'Administrator',
    summary: 'Administrator account',
    role: 'Admin',
    email: 'admin@example.com',
    password: hashedPassword,
    created_at: new Date(),
    updated_at: new Date(),
  }]);
};

export const down: MigrationFn<Sequelize> = async ({ context }) => {
  const q = context.getQueryInterface();

  await q.bulkDelete('users', {
    email: 'admin@example.com',
  });
};