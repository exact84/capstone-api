import {Context, RouterFactory} from '../interfaces/general';
import express, {Request, Response, NextFunction} from 'express';
import { check, validationResult } from "express-validator";
// import {logger} from '../libs/logger';

export const makeAuthRouter: RouterFactory = (context: Context) => {
  const router = express.Router();
  const authService = context.services.authService;

  router.post("/register", [
    check('firstName').notEmpty(),
    check('lastName').notEmpty(),
    check('title').notEmpty(),
    check('summary').notEmpty(),
    check('email').notEmpty().isEmail(),
    check('password').notEmpty()
    ],
  async (req: Request, res: Response, next: NextFunction) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
    await authService.registerUser(req, res, next)
    });

  router.post("/login", [ 
      check('email').notEmpty(),
      check('password').notEmpty()
    ],
    async(req: Request, res: Response, next: NextFunction) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }
      await authService.login(req, res, next)
  });

  return router;
}
