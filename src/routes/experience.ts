import {Context, RouterFactory} from '../interfaces/general';
import express, {Request, Response} from 'express';
import { check, validationResult } from "express-validator";

import {roles} from '../middleware/roles';
import {SerializedExperience} from '../models/experience.model';
import {UserRole} from '../models/user.model';
import {errorHandlerDecorator} from '../middleware/errorHandler';
import {logger} from '../libs/logger';

export const makeExperienceRouter: RouterFactory = (context: Context) => {
  const router = express.Router();
  const experienceService = context.services.experienceService;

  router.post("/", [
    check('userId').notEmpty(),
    check('companyName').notEmpty(),
    check('startDate').notEmpty().isDate(),
    check('endDate').notEmpty().isDate(),
    check('description').notEmpty(),
    ], errorHandlerDecorator(async (req: Request, res: Response) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        logger.info('Невалидные данные');
        return res.status(400).json({ errors: errors.array() });
      }

      const exp: SerializedExperience =
      {
        // id: 0,
        userId: req.body.userId,
        companyName: req.body.companyName,
        role: req.body.role,
        startDate: req.body.startDate, 
        endDate: req.body.endDate,
        description: req.body.description
    } 
    const newExp = await experienceService.postExp(exp)
    return res.status(201).json(newExp);
}));

router.get("/", roles([UserRole.Admin]), errorHandlerDecorator(async (req: Request, res: Response) => { // roles,
  const pageSize: number = parseInt(req.query.pageSize as string) || 10; // Размер страницы по умолчанию 10
  const page: number = parseInt(req.query.page as string) || 1; // Номер страницы по умолчанию 1
  if (isNaN(pageSize) || isNaN(page) || pageSize <= 0 || page <= 0) {
    return res.status(400).json({ message: 'Неверные параметры запроса' });
  }

    const { exp, totalCount }: {exp: SerializedExperience[], totalCount: number} = 
        await experienceService.getExp({pageSize, page});
    res.set('X-Total-Count', totalCount.toString());
    res.status(200).json(exp);

}));

  return router;
}
