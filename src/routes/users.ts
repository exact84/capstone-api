import {Context, RouterFactory} from '../interfaces/general';
import express, {Request, Response, NextFunction} from 'express';
import { check, validationResult } from "express-validator";
import {roles} from '../middleware/roles';
import { UserRole, SerializedUser } from '../models/user.model';
import { logger } from '../libs/logger';

export const makeUsersRouter: RouterFactory = (context: Context) => {
  const router = express.Router();
  const usersService = context.services.usersService;

  router.post("/", [
    check('firstName').notEmpty(),
    check('lastName').notEmpty(),
    check('title').notEmpty(),
    check('summary').notEmpty(),
    check('email').notEmpty().isEmail(),
    check('password').notEmpty(),
    ],
    roles([UserRole.Admin]), 
    async (req: Request, res: Response, next: NextFunction) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        logger.info('Невалидные данные');
        return res.status(400).json({ errors: errors.array() });
      }
      try {
        const user = await usersService.postUser(req, res);
        return res.status(201).json(user);
      }
      catch(err) {
        next(err);
      }
  });

  router.get("/", roles([UserRole.Admin]), async (req: Request, res: Response) => { // roles,
    const pageSize: number = parseInt(req.query.pageSize as string) || 10; // Размер страницы по умолчанию 10
    const page: number = parseInt(req.query.page as string) || 1; // Номер страницы по умолчанию 1
    if (isNaN(pageSize) || isNaN(page) || pageSize <= 0 || page <= 0) {
      return res.status(400).json({ message: 'Неверные параметры запроса' });
    }
    try {
      const { users, totalCount }: {users: SerializedUser[], totalCount: number} = await usersService.getUsers({pageSize, page});
      res.set('X-Total-Count', totalCount.toString());
      res.json(users);
    } catch (error) {
      res.status(500).json({ message: 'Ошибка при получении пользователей' });
    }
  });

  router.get("/:id", async (req: Request, res: Response) => { 
    // console.log('router get user by id', req.params.id);
    try {
      const userId: number = parseInt(req.params.id);
      if (isNaN(userId)) {
        return res.status(400).send('Invalid user ID');
      }
      // logger.info(userId, id);
      const user: SerializedUser = await usersService.getUserById(userId);
      // if (!user) return res.status(404).send('User not found.');
      res.json(user);
  } catch (error) {
    res.status(404).json({ message: 'User not found.' });
  }
  });

  router.put("/:id",
     [
    check('firstName').notEmpty(),
    check('lastName').notEmpty(),
    check('title').notEmpty(),
    check('summary').notEmpty(),
    check('email').notEmpty().isEmail(),
    check('password').notEmpty(),
    ], 
    async (req: Request, res: Response) => {
    try {
      const userId = parseInt(req.params.id); 
      // console.log(userId);
      if (isNaN(userId)) {
        return res.status(400).send('Invalid user ID');
      }
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        logger.info('Невалидные данные');
        return res.status(400).json({ errors: errors.array() });
      }
    // if (req.user) {
      const id = (req.user as { id: number }).id; 
      // console.log(id);
      if (id == userId) {
        const user: SerializedUser = await usersService.putUser(req, res, userId);
        res.status(200).json(user);
      }
    // }
    else   
      res.status(400).json({Msg: 'User has no rights for this action'});
    } catch (error) {
      logger.error(error);
      res.status(500).send('Internal Server Error');
    }
  });

  router.delete("/:id", roles([UserRole.Admin]), async (req: Request, res: Response) => {
    const userId = parseInt(req.params.id); 
    if (isNaN(userId)) {
      return res.status(400).send('Invalid user ID');
    }
    const count: number = await usersService.deleteUser(userId)
    res.status(204).send(`${count} record was deleted`);
  });

  return router;
}
