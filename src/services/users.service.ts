import {Request, Response} from 'express';
import bcrypt from "bcrypt";

import {User, UserRole, SerializedUser} from "../models/user.model";
// import {config} from "../config/";
import {logger} from '../libs/logger';

export class UsersService {

  public async postUser(req: Request, res: Response): Promise<SerializedUser>{

    const { firstName, lastName, title, summary, email, password, role }: 
    {firstName: string, lastName: string, title: string, summary: string, 
      email: string, password: string, role: UserRole} = req.body;

    // Если роль не указана - то это Юзер
    // const role: UserRole = req.body.role ? req.body.role : UserRole.User;
    // console.table({firstName, lastName, title, summary, email, password, role} );
    let avatarPath: string = 'empty'; 
    if (req.file) avatarPath = req.file.path;

    // Проверяем email на уникальность
    const candidate: User | null = await User.findOne({
      where: { email: email },
    });
    if (candidate) {
      logger.info({ requestId: req.id }, "Validation failed. email should be a unique field.");
      res.status(400).json({ message: "Validation failed. email should be a unique field." });
      return
    }

    // создаём хеш пароля, и сохраняем пользователя
    const hash = await bcrypt.hash(password, 12);
    const user = await User.create({
      firstName: firstName,
      lastName: lastName,
      image: avatarPath,
      title: title,
      summary: summary,
      email: email,
      role: role,
      password: hash,
    });

    if (user) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { password, updatedAt, createdAt, ...data } = user.dataValues;
      console.log(user); 
      logger.info({ requestId: req.id }, 'User added.')
      return data; 
    }
  }

  public async getUsers({ pageSize, page }: { pageSize: number; page: number }): Promise<{users: SerializedUser[], totalCount: number}>{
//  Request queries:
// pageSize: number	The size of a page returned to a caller. 
//          Max number of rows in a response.
// page: number	The number of the page.
    const users: User[] = await User.findAll({ limit: pageSize, offset: (page - 1) * pageSize});
    const totalCount: number = await User.count();
    // const { password, updatedAt, createdAt, ...data } = user.dataValues;
    const serializedUsers: SerializedUser[] = users.map(this.serializeUser);
    return { users: serializedUsers, totalCount };
  }

  public async getUserById( userId : number): Promise<SerializedUser>{
    // console.info({ requestId: req.id }, 'getUser method');
    const user = await User.findOne({ where: { id: userId } });
    return this.serializeUser(user) ;
  }

  public async putUser(req: Request, res: Response, id: number): Promise<User>{
    const { firstName, lastName, title, summary, email, password, role}: 
    {firstName: string, lastName: string, title: string, summary: string, 
      email: string, password: string, role: UserRole} = req.body;
    // создаём хеш пароля, и сохраняем пользователя
    const hash = await bcrypt.hash(password, 12);
    await User.update({
      firstName: firstName,
      lastName: lastName,
      title: title,
      summary: summary,
      email: email,
      role: role,
      password: hash,
    }, {where: {id: id}});
    const user = await User.findOne({ where: { id: id } });
    // console.table(user);
    return user;
  }

  public async deleteUser(id: number): Promise<number>{
    const deleted = await User.destroy({
      where: { id: id },
    });
    return deleted;
  }

  async findUserByEmail(email: string): Promise<User | null>{
      // Поиск по email для passport
      const user = await User.findOne({ where: { email } });
      return user;
    }

  private serializeUser = (user: User) => {
    // Чтобы отправлять пользователю только нужные поля
      const { id, firstName, lastName, image, title, summary, role, email } = user.get({ plain: true });
      return { id, firstName, lastName, image, title, summary, role, email };
    }; 

}
