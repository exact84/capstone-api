import {Request, Response} from 'express';

// import {User, UserRole} from "../models/user.model";
// import {config} from "../config/";
// import {logger} from '../libs/logger';

export class ProjectsService {

    public postUser(req: Request, res: Response): Promise<void>{
        res.status(201).end;
        return;
    }

    public getUser(req: Request, res: Response): Promise<void>{
        res.status(200).end;
        return;
    }

    public putUser(req: Request, res: Response): Promise<void>{
        res.status(200).end;
        return;
    }

    public deleteUser(req: Request, res: Response): Promise<void>{
        res.status(204).end;
        return;
    }
}
