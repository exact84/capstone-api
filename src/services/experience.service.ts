import {Request, Response} from 'express';
import {Experience, SerializedExperience, } from '../models/experience.model';
// import {config} from "../config/";
// import {logger} from '../libs/logger';

export class ExperienceService {

    public async postExp(exp: SerializedExperience): Promise<SerializedExperience>{
        
        const expReturn = await Experience.create({
            userId: exp.userId,
            companyName: exp.companyName,
            role: exp.role,
            startDate: exp.startDate,
            endDate: exp.endDate,
            description: exp.description
        });

        const { id, userId, companyName, role, startDate, endDate, description } = expReturn.get({ plain: true });
        return { id, userId, companyName, role, startDate, endDate, description };
    }

    public async getExp({ pageSize, page }: { pageSize: number; page: number }): Promise<{exp: SerializedExperience[], totalCount: number}>{
        const exp: Experience[] = await Experience.findAll({ limit: pageSize, offset: (page - 1) * pageSize});
        const totalCount: number = await Experience.count();
        const serializedExp: SerializedExperience[] = exp.map(exp => {
            // throw new Error("Отработка ошибок");
            const { id, userId, companyName, role, startDate, endDate, description } = exp.get({ plain: true });
            return { id, userId, companyName, role, startDate, endDate, description };

          });
        return {exp: serializedExp, totalCount};
    }

    public putExp(req: Request, res: Response): Promise<void>{
        res.status(200).end;
        return;
    }

    public deleteExp(req: Request, res: Response): Promise<void>{
        res.status(204).end;
        return;
    }
}
