import passport from "passport";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import {NextFunction, Request, Response} from 'express';

import {User, UserRole} from "../models/user.model";
import {config} from "../config/";
import {logger} from '../libs/logger';

export class AuthService {

  public async registerUser(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const { firstName, lastName, title, summary, email, password }: 
        {firstName: string, lastName: string, title: string, summary: string, 
          email: string, password: string} = req.body;
      let avatarPath: string = 'empty'; 
      if (req.file) avatarPath = req.file.path;

      // Проверяем email на уникальность
      const candidate: User | null = await User.findOne({
        where: { email: email },
      });
      if (candidate) {
        logger.info({ requestId: req.id }, "Validation failed. email should be a unique field.");
        res.status(400).json({ message: "Validation failed. email should be a unique field." });
        return
      }

      // создаём хеш пароля и сохраняем пользователя
      const hash = await bcrypt.hash(password, 12);
      // console.log(firstName, lastName, title, summary, email, password);
      const user = await User.create({
          firstName: firstName,
          lastName: lastName,
          image: avatarPath,
          title: title,
          summary: summary,
          role: UserRole.User,
          email: email,
          password: hash,
      });

      if (user) {
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          const { password, updatedAt, createdAt, ...data } = user.dataValues;
          res.status(201).json(data); 
          logger.info({ requestId: req.id }, 'User added.')
      }
    }
    catch(err) {
      logger.error({ requestId: req.id }, 'Internal error. User not saved.')
      next(err);
    }
  }

  public async login(req: Request, res: Response, next: NextFunction): Promise<void> {

    passport.authenticate("local", (err, user, info) => {
      if (err || !user) {
        return res.status(400).json(info);
      }

      req.login(user, { session: false }, (err) => {
        if (err) {
          logger.error({ requestId: req.id }, 'Login error', err);
          return res.status(400).json(err);
        }
        const token = jwt.sign({email: user.email, role: user.role}, config.auth.secret, { expiresIn: '24h' });
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const { password, updatedAt, createdAt, ...data } = user;
        logger.info({ requestId: req.id },'Success login user');
        return res.status(200).json({ data, token });
      });
    })(req, res, next);
  }

  public generateToken(payload: object) {
    const token: string = jwt.sign(payload, config.auth.secret, { expiresIn: "12h" });
    return token;
  }
}
