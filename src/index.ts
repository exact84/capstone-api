import { loadApp } from './loaders/app';

const PORT = process.env.PORT || 3001;

(async () => {
  const app = await loadApp();

  app.listen(PORT, () =>
    console.log(`Application is running on http://localhost:${PORT}`),
  );
})();
