import { DataTypes, Model, Optional, Sequelize } from 'sequelize';
import { Models } from '../interfaces/general';

interface FeedbackAttributes {
  id: number;
  fromUser: number;
  toUser: number;
  content: string;
  companyName: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export class Feedback
  extends Model<FeedbackAttributes, Optional<FeedbackAttributes, 'id'>>
  implements FeedbackAttributes
{
  public id!: number;
  public fromUser!: number;
  public toUser!: number;
  public content!: string;
  public companyName!: string;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  static defineSchema(sequelize: Sequelize) {
    Feedback.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        fromUser: {
          type: DataTypes.INTEGER.UNSIGNED,
          allowNull: false,
        },
        toUser: {
          type: DataTypes.INTEGER.UNSIGNED,
          allowNull: false,
        },
        content: {
          type: DataTypes.TEXT,
          allowNull: false,
        },
        companyName: {
          type: new DataTypes.STRING(128),
          allowNull: false,
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false,
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false,
        },
      },
      {
        tableName: 'feedbacks',
        underscored: true,
        sequelize,
      },
    );
  }

  static associate(models: Models) {
    Feedback.belongsTo(models.user, {
      foreignKey: 'from_user',
      as: 'from_User',
    });
    Feedback.belongsTo(models.user, {
      foreignKey: 'to_user',
      as: 'to_User',
    });
  }
}
