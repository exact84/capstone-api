import passport from "passport";
import { Strategy as JWTStrategy, ExtractJwt } from "passport-jwt";
import { Strategy as LocalStrategy } from "passport-local";
import bcrypt from "bcrypt";

import {config} from "../config/";
import { UsersService } from "../services/users.service";


export function setupPassport(usersService: UsersService ) {
  passport.use(
    new JWTStrategy(
      {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: config.auth.secret,
      },
      async function (jwtPayload, done) {
        try {
          const user = await usersService.findUserByEmail(jwtPayload.email);
          if (!user) {
            // console.log(`User ${jwtPayload.email} not found.`);
            return done(null, false); //null
          }

          // Просто исключаем пароль чтоб не отправлять пользователю.
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          const { password, ...data } = user.dataValues;
          done(null, data);
        } catch (e) {
          done(e);
        }
      }
    )
  );

  passport.use(
    new LocalStrategy(
      {
        usernameField: "email",
        passwordField: "password",
      },
      async function (email: string, password: string, done) {
        try {
          const user = await usersService.findUserByEmail(email);

          if (!user) {
            return done(null, false, { message: "User with provided email not found!"});
          }

          const validPassword: boolean = await bcrypt.compare(
            password,
            user.password
          );

          if (!validPassword) {
            return done(null, false, {
              message: "Invalid password!",
            });
          }
          // Просто исключаем пароль чтоб не отправлять пользователю.
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          const { password: userPassword, ...userData } = user.dataValues;
          return done(null, userData, {
            message: "User logged in successfully!",
          });
        } catch (e) {
          return done(e);
        }
      }
    )
  );
};
